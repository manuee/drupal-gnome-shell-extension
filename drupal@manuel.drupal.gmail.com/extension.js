const Main = imports.ui.main;
const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const DrupalMenu = Extension.imports.drupalMenu;

let drupalMenu;

// init with translation support
function init() {
  Convenience.initTranslations("drupal@manuel.drupal.gmail.com");
}

// Build and add the extension
function enable() {
    drupalMenu = new DrupalMenu.DrupalMenuButton();
    Main.panel.addToStatusArea('drupalMenu', drupalMenu);
}

function disable() {
    drupalMenu.destroy();
}
