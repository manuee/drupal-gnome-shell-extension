const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;
const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const Gettext = imports.gettext.domain("drupal@manuel.drupal.gmail.com");
const _ = Gettext.gettext;
const Lang = imports.lang;

const SETTING_DRUPAL_INSTALL_DIR = 'drupal-install-dir';

const drupalPrefsWidget = new GObject.Class({
    Name: 'drupalPrefsWidget',
    GTypeName: 'drupalPrefsWidget',
    Extends: Gtk.Grid,

    _init: function(params) {
        this.parent(params);
        this.orientation = Gtk.Orientation.VERTICAL;
        this.margin = 12;
        this._settings = Convenience.getSettings();

        this._widgets = {};

        this._widgets.box = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL,
                            margin: 20,
                            margin_top: 10,
                            expand: true,
                             spacing: 10
        });

        this._addDrupalInstallDirFileChooserButton();

        this.add(this._widgets.box);
    },

    _addDrupalInstallDirFileChooserButton: function() {
        let hbox = new Gtk.Box({orientation: Gtk.Orientation.HORIZONTAL});
        let label = new Gtk.Label({label: _("Drupal installation directory"),
                                        xalign: 0});

        let fileChooserButton;
        // Create a directory chooser button.
        fileChooserButton = new Gtk.FileChooserButton({
            'title': 'Select the root of a Drupal installation',
            'action': Gtk.FileChooserAction.SELECT_FOLDER,
         });
        fileChooserButton.set_current_folder(this._settings.get_string(SETTING_DRUPAL_INSTALL_DIR));
        // Bind the directory selection to our settings.
        fileChooserButton.connect('selection-changed', Lang.bind(this, function(entry) {
            this._settings.set_string(SETTING_DRUPAL_INSTALL_DIR, entry.get_filename());
        }));

        // Add the button to the UI
        this._widgets.drupalInstallDirFileChooserButton = fileChooserButton;
        hbox.pack_start(label, true, true, 0);
        hbox.add(this._widgets.drupalInstallDirFileChooserButton);
        this._widgets.box.add(hbox);
    }

});

function init() {
   Convenience.initTranslations("drupal@manuel.drupal.gmail.com");
}

function buildPrefsWidget() {
    let widget = new drupalPrefsWidget();
    widget.show_all();

    return widget;
}
