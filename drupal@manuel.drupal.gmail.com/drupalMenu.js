const St = imports.gi.St;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const Util = imports.misc.util;
const Tweener = imports.ui.tweener;

// import custom files
const Extension = imports.misc.extensionUtils.getCurrentExtension();

// translation support
const Convenience = Extension.imports.convenience;
const Gettext = imports.gettext.domain("drupal@manuel.drupal.gmail.com");
const _ = Gettext.gettext;

// Settings
const SETTING_DRUPAL_INSTALL_DIR = 'drupal-install-dir';

// Helper functions to show messages on actions taken
let text;
function _hideMessage() {
    Main.uiGroup.remove_actor(text);
    text = null;
}

function _showMessage(text_to_display) {
    if (!text) {
        text = new St.Label({ style_class: 'message-label', text: text_to_display });
        Main.uiGroup.add_actor(text);
    }
    text.opacity = 255;
    let monitor = Main.layoutManager.primaryMonitor;
    text.set_position(monitor.x + Math.floor(monitor.width / 2 - text.width / 2),
                      monitor.y + Math.floor(monitor.height / 2 - text.height / 2));
    Tweener.addTween(text,
                     { opacity: 0,
                       time: 4,
                       transition: 'easeOutQuad',
                       onComplete: _hideMessage });
}



const DrupalMenuButton = new Lang.Class({
    Name: 'Drupal Button',
    Extends: PanelMenu.Button,

    _init: function () {
        this.parent(0.0, Extension.name);

        // Read settings
        this._settings = Convenience.getSettings();
        this.drupalInstallDir = this._settings.get_string(SETTING_DRUPAL_INSTALL_DIR)

        // path for icon
        Gtk.IconTheme.get_default().append_search_path(Extension.dir.get_child('icons').get_path());

        // Icon for the Panel
        this.drupalIcon = new St.Icon({
            icon_name: 'druplicon',
            style_class: 'system-status-icon'
        });

        this.actor.add_actor(this.drupalIcon);
        this.actor.add_style_class_name('panel-status-button');

        // Box for the Control Elements
        this.controlsBox = new St.BoxLayout({
            name: 'controlsBox',
            style_class: 'control-box',
            width: 300
        });

        // Current InstallDir Label
        this.drupalInstallDirLabel = new St.Label({
            text: this.drupalInstallDir,
            style_class: 'drupal-user-label'
        });
        // We call this here because _setInstallDirLabel() will set a helpful
        // message in case the setting is empty.
        this._setInstallDirLabel();

        // Add Label to the BoxLayout
        this.controlsBox.add(this.drupalInstallDirLabel);

        // Add ControlsBox to the Menu
        this.menu.box.add_child(this.controlsBox);

        // PopupSeparator
        let separator1 = new PopupMenu.PopupSeparatorMenuItem();
        this.menu.addMenuItem(separator1);

        // Main buttons for Drupal
        this.operationsItem = new PopupMenu.PopupBaseMenuItem({
            reactive: false,
            can_focus: false
        });

        // Clear cache button
        this.cacheClearIcon = new St.Icon({
            // icon_name: 'view-refresh-symbolic'
            icon_name: 'edit-clear-all-symbolic'
        });
        this.cacheClearButton = new St.Button({
            style_class: 'system-menu-action',
            label: 'Clear cache',
            can_focus: true
        });
        this.cacheClearButton.set_child(this.cacheClearIcon);
        this.cacheClearButton.connect('clicked', Lang.bind(this, function() {
            this.menu.close();
            this._clearCache();
        }));
        this.operationsItem.actor.add(this.cacheClearButton, {expand: true, x_fill: false});

        // Get a user login button
       this.userLoginIcon = new St.Icon({
            // icon_name: 'view-refresh-symbolic'
            icon_name: 'avatar-default-symbolic'
        });
        this.userLoginButton = new St.Button({
            style_class: 'system-menu-action',
            label: 'User login',
            can_focus: true
        });
        this.userLoginButton.set_child(this.userLoginIcon);
        this.userLoginButton.connect('clicked', Lang.bind(this, function() {
            this.menu.close();
            this._userLogin();
        }));
        this.operationsItem.actor.add(this.userLoginButton, {expand: true, x_fill: false});

        this.menu.addMenuItem(this.operationsItem);

        // PopupSeparator
        let separator2 = new PopupMenu.PopupSeparatorMenuItem();
        this.menu.addMenuItem(separator2);

         // Settings button
        this.settingsItem = new PopupMenu.PopupBaseMenuItem({
            reactive: false,
            can_focus: false
        });
        this.settingsIcon = new St.Icon({
            icon_name: 'preferences-system-symbolic'
        });
        this.settingsButton = new St.Button({
            style_class: 'system-menu-action',
            can_focus: true
        });
        this.settingsButton.set_child(this.settingsIcon);
        this.settingsButton.connect('clicked', Lang.bind(this, function() {
            this.menu.close();
            this._openPrefs();
        }));
        this.settingsItem.actor.add(this.settingsButton, {expand: true, x_fill: false});
        this.menu.addMenuItem(this.settingsItem);

        // Setting change
        this._settings.connect("changed::" + SETTING_DRUPAL_INSTALL_DIR, Lang.bind(this, function() {
            this.drupalInstallDir = this._settings.get_string(SETTING_DRUPAL_INSTALL_DIR);
            this._setInstallDirLabel();
        }));
    },

    _setInstallDirLabel: function() {
        if (this.drupalInstallDir !== undefined && this.drupalInstallDir) {
            this.drupalInstallDirLabel.set_text(this.drupalInstallDir);
        }
        else {
            this.drupalInstallDirLabel.set_text('No site path configured.');
        }
    },

    _openPrefs: function() {
        let _appSys = Shell.AppSystem.get_default();
        let _gsmPrefs = _appSys.lookup_app('gnome-shell-extension-prefs.desktop');

        if (_gsmPrefs.get_state() == _gsmPrefs.SHELL_APP_STATE_RUNNING) {
            _gsmPrefs.activate();
        } else {
            let info = _gsmPrefs.get_app_info();
            let timestamp = global.display.get_current_time_roundtrip();
            let metadata = Extension.metadata;
            info.launch_uris([metadata.uuid], global.create_app_launch_context(timestamp, -1));
        }
    },

    _executeDrushCommand: function(command) {
        // Util.spawn(['/bin/bash', '-c', "xrandr --query | awk 'something'"])
        Util.spawn(['/bin/bash', '-c', "cd " + this.drupalInstallDir + '&& drush ' + command]);
    },

    _clearCache: function() {
        // Clear cache on this.drupalInstallDir
        this._executeDrushCommand('cr --notify');
        _showMessage('Clearing cache on ' + this.drupalInstallDir + '...')
    },

    _userLogin: function() {
        // Get a user 1 session on this.drupalInstallDir
        this._executeDrushCommand('uli');
        _showMessage('Getting a user login session on ' + this.drupalInstallDir + '...')
    }
});
