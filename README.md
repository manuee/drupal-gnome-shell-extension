# Drupal

This GNOME shell extension displays has a popup menu which allows you to perform
certain actions on a local Drupal site.

![Screenshot](/screenshot.jpg?raw=true "Drupal gnome-shell extension")

## Installation

This extension assumes that [drush](https://www.drupal.org/project/drush) is able to run on the Drupal site configured.

Simply place the drupal@manuel.drupal.gmail.com directory into your gnome shell
extensions directory.

On Ubuntu this can be the .local/share/gnome-shell/extensions directory on your
home folder if you just want your user to have it, or if you want all users
to have access to it place it on /usr/share/gnome-shell/extensions/.

Once installed, configure the root folder of the drupal site you want to run the
actions on by going to the settings in the dropdown menu.

## Authors

  * [Manuel Garcia](https://www.drupal.org/user/213194)

## License

Copyright (C) 2017 Manuel Garcia Ruiz de Leon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.